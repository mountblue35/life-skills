# Apache Kafka

## Introduction

* Apache Kafka is an open-source distributed event streaming platform and a robust queue that can handle a high volume of data and enables you to pass messages from one end-point to another. [^1] 
* It integrates very well with Apache Storm and Spark for real-time streaming data analysis.  [^2]

### What is event streaming?

* Event streaming is the practice of capturing data in real-time from event sources like-
    * databases
    * sensors
    * mobile devices
    * cloud services
    * software applications 

It does so in the form of streams of events; storing these event streams durably for later retrieval; manipulating, processing, and reacting to the event streams in real-time as well as retrospectively; and routing the event streams to different destination technologies as needed. [^3]

* Kafka is a distributed event streaming platform that lets you read, write, store, and process events (also called records or messages in the documentation) across many machines.

* Example events are payment transactions, geolocation updates from mobile phones, shipping orders, sensor measurements from IoT devices or medical equipment, and much more. 
* These events are organized and stored in topics. Very simplified, a topic is similar to a folder in a filesystem, and the events are the files in that folder. 
[^4]

### Why Kafka?

![Image 1](/Technical Communications/Screenshot_from_2022-11-25_18-21-33.png)    [^5]


#### Benefits
* **Reliability** - Kafka is an open-source, distributed, partitioned, replicated and fault-tolerant.
* **Horizontal Scalability** - 
    * Can scale to hundreds of brokers.
    * Can scale to millions of messages per second.
* **Durability** - Kafka uses a 'Distributed commit log' which means messages persist on the disk.
* **Performance** - Kafka has high throughput for publishing and subscribing messages. Latency of less than 10ms. [^2] [^5]
 
### Use Cases

Here are a few of the popular use cases for Apache Kafka. [^6]

1. Messaging
2. Website Activity Tracking
3. Metrics 
4. Log Aggregation
5. Stream Processing
6. Event Sourcing
7. Commit Log
8. Integration with Spark, Flink, Hadoop, Storm etc. [^5] 

For an overview of a number of these areas in action, see [this blog post](https://engineering.linkedin.com/distributed-systems/log-what-every-software-engineer-should-know-about-real-time-datas-unifying).

### Important Server Concepts:                      

1. **Topic partition:** Kafka topics are divided into a number of partitions, which allows you to split data across multiple brokers.
2. **Consumer Group:** A consumer group includes the set of consumer processes that are subscribing to a specific topic.
3. **Node: ** A node is a single computer in the Apache Kafka cluster.
4. **Replicas:** A replica of a partition is a “backup” of a partition. Replicas never read or write data. They are used to prevent data loss.
5. **Producer:** Application that sends the messages.
6. **Consumer:** Application that receives the messages. [^7]

### APIs 


* Kafka includes five core APIs:

    1. The **Producer API** allows applications to send streams of data to topics in the Kafka cluster.
    2. The **Consumer API** allows applications to read streams of data from topics in the Kafka cluster.
    3. The **Streams API** allows transforming streams of data from input topics to output topics.
    4. The **Connect API** allows implementing connectors that continually pull from some source system or application into Kafka or push from Kafka into some sink system or application.
    5. The **Admin API** allows managing and inspecting topics, brokers, and other Kafka objects. [^8]


## Quick Start & Demo  
[^9]

### Installation

**NOTE:**  Our local environment must have Java 8+ installed.

#### Step 1: Verifying Java Installation

```
$ java -version
```

#### Step 2: Get Kafka

[Download](https://www.apache.org/dyn/closer.cgi?path=/kafka/3.3.1/kafka_2.13-3.3.1.tgz) the latest Kafka release and extract it:

    $ tar -xzf kafka_2.13-3.3.1.tgz
    $ cd kafka_2.13-3.3.1

#### Step 3: Start the Kafka Environment

* Apache Kafka can be started using ZooKeeper or KRaft. To get started with either configuration follow one of the sections below but not both.

##### Kafka with ZooKeeper

* Run the following commands to start all services in the correct order:

```
# Start the ZooKeeper service
$ bin/zookeeper-server-start.sh config/zookeeper.properties
```

* Open another terminal session and run:

```
# Start the Kafka broker service
$ bin/kafka-server-start.sh config/server.properties
```

* Once all services have successfully launched, we will have a basic Kafka environment running and ready to use.

##### Kafka with KRaft

* Generate a Cluster UUID

```
$ KAFKA_CLUSTER_ID="$(bin/kafka-storage.sh random-uuid)"
```

* Format Log Directories

```
$ bin/kafka-storage.sh format -t $KAFKA_CLUSTER_ID -c config/kraft/server.properties
```
* Start the Kafka Server

```
$ bin/kafka-server-start.sh config/kraft/server.properties
```

* Once the Kafka server has successfully launched, we will have a basic Kafka environment running and ready to use.

#### STEP 4: CREATE A TOPIC TO STORE OUR EVENTS

* Before we can write our first events, we must create a topic. Open another terminal session and run:

```
$ bin/kafka-topics.sh --create --topic quickstart-events --bootstrap-server localhost:9092
```

#### STEP 5: WRITE SOME EVENTS INTO THE TOPIC

* A Kafka client communicates with the Kafka brokers via the network for writing (or reading) events. 
* Once received, the brokers will store the events in a durable and fault-tolerant manner for as long as we need (even forever).

* Let's Run the console producer client to write a few events into our topic. By default, each line we enter will result in a separate event being written to the topic.

```
$ bin/kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092
This is my first event
This is my second event
```

* We can stop the producer client with Ctrl+C at any time.

#### STEP 6: READ THE EVENTS

* Open another terminal session and run the console consumer client to read the events we just created:

```
$ bin/kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092
This is my first event
This is my second event
```

#### STEP 7: TERMINATE THE KAFKA ENVIRONMENT

* After performing all the operations, we can stop the server using the following command:

```
$ bin/kafka-server-stop.sh config/server.properties
```




## References

[^1]: https://kafka.apache.org/

[^2]: https://www.tutorialspoint.com/apache_kafka/apache_kafka_introduction.htm

[^3]: https://kafka.apache.org/documentation/#intro_streaming

[^4]: https://kafka.apache.org/documentation/#quickstart_createtopic

[^5]: https://www.youtube.com/watch?v=PzPXRmVHMxI

[^6]: https://kafka.apache.org/documentation/#uses

[^7]: https://www.tutorialspoint.com/apache_kafka/apache_kafka_fundamentals.htm

[^8]: https://kafka.apache.org/documentation/#api

[^9]: https://kafka.apache.org/documentation/#quickstart
