# Good Practices for Software Development

## Review Questions

### Question 1

What is your one major takeaway from each one of the 6 sections? So 6 points in total.

1. Some companies use tools like Trello and Jira for documentation. But if your team is not using any such tool, you should write down the requirements and share them with the team. This will help you get immediate feedback. Also, this will serve as a reference for future conversations.

2. Use group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.

3. Look at the way issues get reported in large open-source projects.

4. Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.

5. It will not always be possible for your team members to get on a call with you. Instead, you can send a slack or Whatsapp message. But instead of bombarding them with many messages, you can write down all the questions you have and send them as a single message.

6. Programming is essentially a result of your sustained and concentrated attention

### Question 2

Which area do you think you need to improve on? What are your ideas to make progress in that area?

1. Under-communication when stuck on something:

   - Make sure I'm clear about the requirements by making notes during the meeting.

   - Ask for help in the right manner without hesitating and wasting time.

2. Losing concentration:

   - Remove the distractions like a phone from the work setup or at least put the phone on DND.

   - Use scheduler and deep-work techniques to appropriately utilize time and meet deadlines.
