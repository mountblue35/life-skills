# Learning Process

## 1. How to Learn Faster with the Feynman Technique
Video - 5:47 minutes - [https://www.youtube.com/watch?v=_f-qkGJBPts](https://www.youtube.com/watch?v=_f-qkGJBPts)

### Question 1

There is a famous quote that goes- "If you can't explain it simply, then you don't understand it well enough". 

Richard Feynman was a remarkable physicist and a brilliant teacher and a great explainer. He perfected a technique that can help anyone learn any concept easily by pretending to teach it to someone else. By attempting to explain a concept in simple terms, you’ll quickly realize the areas where you have a good understanding of that concept and also the areas that still require more learning at your end. Because they’ll be the areas where you either get stuck or end up resorting to using complex language and terminology.

The Feynman technique is divided into four steps-

1. Take a piece of paper and write the concept's name at the top.

2. Explain the concept using simple language. 

3. Identify problem areas, then go back to the sources to review. 

4. Pinpoint any complicated terms and challenge yourself to simplify them.

Note: While explaining challenge your assumptions by asking "Why?". It helps you delve deeper into topics you might have accepted at face value.

### Question 2
"If you want to understand something well, try to explain it simply."

The Foundation of this technique involves explaining the concept, you could execute it in a number of ways – including grabbing a friend/colleague and explaining to them what you’re currently learning.  But you don’t always have willing ears around you every time, so here’s the simpler method that just involves a sheet of paper.

- **Step 1:** Get yourself a sheet of paper and write the name of the concept at the top. It could be any concept related to any field.

- **Step 2:** Explain the concept in your own words as if you were teaching it to someone else. Focus on using simpler language. Challenge yourself to work through examples to ensure you can put the concept to actual use.

- **Step 3:** Review your explanation and identify the weak areas where you feel your explanation is shaky and resorted to using complex crammed language. Once you’ve pinpointed them, go back to the source material or any examples you can find to reinforce your understanding of the concept.

- **Step 4:** Go back to your explanation where you’ve used lots of technical terms or complex language, and challenge yourself to re-write these sections in simpler terms. Make sure your explanation could be understood by someone without the knowledge base you believe you already have.


## 2. Learning How to Learn TED talk by Barbara Oakley
Video - 17:50 minutes - [https://www.youtube.com/watch?v=O96fE1E-rf8](https://www.youtube.com/watch?v=O96fE1E-rf8)

### Question 3
Barbara Oakley, PhD, PE is a professor of engineering at Oakland University in Rochester, Michigan. Dr Oakley flunked her way through high school math and science courses, before enlisting in the U.S. Army immediately after graduation. There she soon realized how her lack of mathematical and technical knowledge was hindering her rise in military ranks and also severely limiting her options elsewhere. Therefore she resolved to go back to school to relearn the same subjects she dreaded before.

A brain is a complex machine but its operation can be simplified in two fundamentally different modes- focus mode and diffused mode (relaxed state). She suggests when you are focusing on learning something new and get stuck, allow yourself to switch to the relaxed or diffused mode and gain a new perspective and then jump back to focusing on a task. She gives an example of Salvador Dali, a famous painter, who would switch between the two modes by taking a nap and getting new ideas. 

She talks about procrastination and how people cope with it by giving up and ending up wasting time. To solve this she suggests the use of the "Pomodoro technique" where you work with undivided attention for 25-minute intervals. 

Some people feel they are slow to solve things when compared to their companions. She explains this as a trade-off where you are getting solid mastery of what you're studying. She suggests using effective studying technique

### Question 4
Some of the ways to improve the learning process-

**Pomodoro:** In Pomodoro, you focus for 25 minutes intervals and get to enjoy short breaks in between these intervals until you complete 4 of these Pomodoro and you get a long break.  
**Studying techniques:** 
- Exercising improves our ability to both learn and to remember. 
- Use flashcards, and colour coding the topics. 
- Look at the page once then look away to test what you can recall. 
- Work on homework several times over days until it flows like a song in your brain 

## 3. Learn Anything in 20 hours
Video - 20 minutes - https://www.youtube.com/watch?v=5MgBikgcWnY

### Question 5
Josh Kaufman is the author of the #1 international bestseller, 'The Personal MBA: Master the Art of Business'. John shares his experience about his newborn baby and how he at first struggled with the change and learning something entirely new. He reads about the topic of learning something new and finds the '10,000-hour rule'.

He talks about K. Anders Ericsson, the originator of the rule. He studied top-level athletes in many sports and realized the more time you spend practising on elements, the better you get in your field. And the people at top of their ultra-competitive field have put in ten thousand hours. People mistook this rule as 'it takes 10,000 hours to learn something new while it's not true.

He shows a plotted graph that suggests how the more practice time you put into a task, the better you get at performing the task. He suggests 20 hours are enough to learn something new efficiently no matter what it is. He suggests ways to approach learning-
- Deconstruct the skill
- Learn enough to self-correct
- Remove the barriers
- Practice at least 20 hours

He wraps up with the quote- "The major barrier to skill acquisition isn't intellectual but it's emotional." Meaning people tend to quit since it's uncomfortable to learn something new.

### Question 6

Steps to take when you approach a new subject- 

1. **Deconstruct the skill:** Look at the subject and break it down into many easily approachable concepts. The more you break apart better you can decide to allot appropriate time to it
2. **Lean enough to self-correct:** Get the necessary resources and learn how to self-edit. Notice when you are making a mistake and try to fix it.
3. **Remove the barriers:** Take out all the distractions that keep you away from learning.
4. **Practice at least 20 hours:** Don't be afraid of looking incompetent and looking stupid while you're new to something. Just stick to the resolve of learning for 20 hours. 


