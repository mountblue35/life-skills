# Prevention Of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

Sexual harassment is any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.

There are generally three forms of sexual harassment behaviour:

- Verbal
- Visual
- Physical

### Verbal harassment includes:

1. Comments about a person's body or clothing.

1. Sexual or gender-based jokes or remarks.

1. Requesting sexual favours or repeatdly asking a person out.

1. Sexual innuendos, threats, spreading rumours about a person's personal or sexual life.

1. Foul and obscene language

### Visual harassment includes:

1. Obscene Posters, drawings/pictures.

2. Screensavers of sexual nature

3. Cartoons, Emails or texts of sexual nature

### Physical harassment includes:

1. Sexual assault

1. Impeding or blocking movement

1. Inappropriate touching such as kissing, hugging, patting, stroking or rubbing.

1. Sexual gesturing, leering or staring.

### Two categories of sexual harassment are:

- Quid Pro Quo
- Hostile work environment

#### Quid Pro Quo

Quid Pro Quo ("this for that") happens when an employer or supervisor uses job rewards or punishment to coerce an employee into a sexual act. It includes offering an employee promotions, bonuses or threatening demotions, firing for not complying with their request.

#### Hostile work environment

A hostile work environment occurs when an employee's behaviour interferes with the work performance of another or creates an intimidating or offensive workplace. This happens when an employee makes repeated sexual comments to the extent that the victim's work performance starts suffering or they decline work opportunities to avoid working with the harasser.

## What would you do if you face or witness any incident or repeated incidents of such behaviour?

Such incidents have to be met with serious consequences but that only happens when we report such incidents by making use of the mechanisms set up by the company to tackle such cases.

These generally could include reporting to the manager or HR manager. If such incidents are not tackled by the responsible sexual prevention committee set up by the company, it is for the victim to decide the next legal steps to take against the harasser depending upon the seriousness of the incident.
