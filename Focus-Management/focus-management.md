# Focus Management

## 1. What is Deep Work

Video: 5:08 minutes - [https://www.youtube.com/watch?v=b6xQpoVgN68](https://www.youtube.com/watch?v=b6xQpoVgN68)

Cal Newport is an associate professor of Computer Science. He also writes about how to focus in the modern world in the age of distractions and social media.

He wrote the book "Deep Work".

### Question 1

What is Deep Work?

Deep work is the ability to concentrate deeply on a difficult task for prolonged periods of time without getting distracted.

## 4. Summary of Deep Work Book

Video: 7:30 minutes- [https://www.youtube.com/watch?v=gTaJhjQHcf8](https://www.youtube.com/watch?v=gTaJhjQHcf8)

### Question 2

Paraphrase all the ideas in the above videos and this one **in detail**.

- The first video talks about the optimal duration of deep work and how long you should work without taking breaks. The optimal timing for deep work should be 45 minutes to 1 hour without any distractions like picking up the phone to check notifications or getting up from the seat.

- The Second video talks about deadlines and their positive side of them. The video tells us that a deadline is good because it gives us a motivational signal. It helps you to do the deep work without having any debate with your brain for breaks.
- The third video tells us the summary of the deep workbook. It tells that one should practice deep work in day-to-day life. The video also talks about the strategies one can implement for deep work:-

  1. Schedule breaks

  2. Make deep work a habit or a sinusoidal function of time
  3. Get adequete sleep

### Question 3 

How can you implement the principles in your day-to-day life?

The steps you can take to implement deep work in your day-to-day life are:-

- At least practice deep work for 1 hour a day

- Schedule distractions
- Practice deep work regularly and make it a habit
- Get adequate sleep

## 5. Dangers of Social Media

Video: 13:50 minutes - [https://www.youtube.com/watch?v=3E7hkPZ-HTk](https://www.youtube.com/watch?v=3E7hkPZ-HTk)

### Question 4

Your key takeaways from the video

Some of the takeaways from the videos are:-

- Social Media is not a fundamental technology and costs the productivity of people badly.

- Social Media is designed to harness your attention and keep you distracted for a while.
- The use of social media affects one's mental health badly.
