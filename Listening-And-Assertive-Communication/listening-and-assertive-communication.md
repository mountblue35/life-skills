# Listening and Active Communication

## 1. Active Listening


#### Question 1

Active Listening is the act of fully listening and comprehending the meaning of what someone else is saying.

Steps to do Active Listening:

1. Avoid getting distracted by your thoughts. Focus on the speaker and topic instead.
2. Try not to interrupt the other person. Let them finish and then respond.
3. Use door openers. These are phrases that show you are interested and keep the other person talking.
4. Show that you are listening with correct body language.
5. If appropriate take notes during meaningful conversations.
6. Paraphrase what others have said to make sure you are on the same page.

[Active Listening](https://www.youtube.com/watch?v=rzsVh8YwZEQ)

## 2. Reflective Listening

Reflective listening is used widely in different situations. It is a subset of Active Listening with more emphasis on mirroring body language, and emotions and verifying the message.

* In critical situations e.g. conversations between Air Traffic Controllers and Pilots, conversations in military missions
* In business meetings - notes are taken by a notetaker and then verified by the participants
* To diffuse emotionally charged conversations in personal relationships

### Application to Business/Software Context

* Always take notes in meetings
* Share the notes with all stakeholders - client, manager or team members and ask them if there is a mistake
* Make sure the team is on the same page i.e. everyone agrees on the points discussed
* Take notes whenever you are in a technical discussion because there are minute details that might slip from the mind when you get down to implementing

#### Question 2

A listener can implement the elements of listening orientation through a method known as reflection. In reflection, the listener tries to clarify and restate what the other person is saying. This can have a threefold advantage: 

1. It can increase the listener's understanding of the other person.
2. It can help the other to clarify their thoughts

3. It can reassure the other that someone is willing to attend to his or her point of view and wants to help. Empathy, acceptance, congruence, and concreteness contribute to the making of reflective responses. 

Some principles of reflective listening:

- More listening than talking.

- Responding to what is personal rather than to what is impersonal, distant, or abstract.

- Restating and clarifying what the other has said, not asking questions or telling what the listener feels, believes, or wants.

- Trying to understand the feelings contained in what the other is saying, not just the facts or ideas.

- Working to develop the best possible sense of the other's frame of reference while avoiding the temptation to respond from the listener's frame of reference.

- Responding with acceptance and empathy, not with indifference, cold objectivity, or fake concern.


## 3. Reflection

#### Question 3

1. More talking than listening.

2. Changing topics.
3. Not responding with appropriate emotions.
4. Not letting the speaker finish what they are saying.
5. Not responding with acceptance, empathy or showing indifference.

#### Question 4

1. Make a constant effort to empathize with the speaker.

2. Trying to understand the feeling contained in what the speaker is saying, not just the facts or ideas.

3. Showing warmth in attitude and not feigning concern.
4. More listening than talking.
5. Restating and clarifying what the speaker has said, not asking questions or telling what the listener feels, believes or wants.


## 4. Types of Communication
* Passive Communication
* Aggressive Communication
* Passive-Aggressive Communication
* Assertive Communication

#### Question 5


Passive communication is a style in which individuals have developed a pattern of avoiding expressing their opinions or feelings, protecting their rights, and identifying and meeting their needs. 

 It is often used by people who want to come off as indifferent about the topic at hand or want to please other people by not keeping their own needs first. They either keep their opinions to themselves or try to make it seem as if they support every piece of input in the discussion. This slowly results in a person becoming a pushover and often disrespected making it very unhealthy for their mental state.


#### Question 6

Aggressive communication is a style in which individuals express their feelings and opinions and advocate for their needs in a way that violates the rights of others. Therefore, aggressive communicators can often be verbally and/or physically abusive. People generally tend to get aggressive with others when they are comfortable with them or with people they don't respect much.

#### Question 7

Passive Aggressive communication is a style in which individuals appear tolerant on the surface but are really acting out anger in a subtle, indirect, or behind-the-scenes way.

 People usually tend to develop a pattern of being passive-aggressive when they feel: 

 * Powerless

 * Stuck 
 * Resentful
 * Incapable of dealing directly with people.
 
  Instead, they express their anger by subtly undermining the person(real or imagined) of their resentments. They tend to make snarky comments and in general, be snide rather than being assertive in their needs and discussing their displeasure

#### Question 8

Assertive communication is a style in which individuals clearly state their opinions and feelings, and firmly advocate for their rights and needs without violating the rights of others.

Actively value yourself, your time, and your emotional, spiritual, and physical needs while being very respectful of the rights of others.

Assertive communication can be practised by:


1. Have good eye contact

2. Learn to recognise and name your feeling before communicating.

3.   Learn to recognize and state your needs and wants clearly, appropriately, and respectfully.

3. Practice with low-stakes situations that you are more comfortable with.

4. Be aware of your body language while communicating. Make sure you don't look too passive or aggressive.

5. Don’t wait to speak up when someone unfairly treats you.