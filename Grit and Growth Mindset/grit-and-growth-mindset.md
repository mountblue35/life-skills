# Grit and Growth Mindset

## 1. Grit
Grit Video - 6:12 minutes - [https://www.youtube.com/watch?v=H14bBuluwB8](https://www.youtube.com/watch?v=H14bBuluwB8)

#### Question 1
Paraphrase (summarize) the video in a few lines. Use your own words.

Grit is passion and perseverance for very long-term goals. Grit is having stamina. Grit is sticking with your future, day in, day out, not just for the week, not just for the month, but for years, and working hard to make that future a reality. Grit is living life like it's a marathon, not a sprint.


#### Question 2
What are your key takeaways from the video to take action on?

Building grit can be fueled by having a growth mindset. The ability to learn is not fixed and can be changed with our effort. When we learn how our brain functions and grows in response to challenges, we are much more likely to persevere as we understand failure is not a permanent condition.  

## 2. Introduction to Growth Mindset

Introduction to Growth Mindset Video - 8:25 minutes - [https://www.youtube.com/watch?v=75GFzikmRY0](https://www.youtube.com/watch?v=75GFzikmRY0)

#### Question 3
Paraphrase (summarize) the video in a few lines in your own words.

'Growth Mindset' is the belief in your capacity to learn and grow. This idea has been pushed forward by Dr Carol Dweck (Professor of Psychology at Stanford University). According to her, a person's mindset plays a crucial role in their ability to learn, develop and eventual success. Two ways to think about learning are:

* Fixed Mindset
* Growth Mindset

| Fixed Mindset   |      Growth Mindset    |
|:----------      |:-----------|
| They believe that skills and intelligence <br>are set and you either have them or you<br> don't- That some people are naturally <br> good at things, while others are not. |  They believe that skills and intelligence<br> are grown and developed. People who are<br> good at something are good because they<br> built that ability and people who aren't-<br> are not good because they haven't<br> done the work.  |
| They believe that you are not in control  <br>of your abilities. | They believe that you are in control of<br> your abilities.     |
| Skills are born.  | Skills are built. |
|You can't learn and grow.   | You can learn and grow.   |

A growth mindset is the foundation of learning.

---
#### Characteristics:
1. Beliefs -  

   * Fixed Mindset: Skills are born. You can't learn and grow.

    * Growth Mindset:  Skills are built. You can learn and grow.

2. Focus - 

    * Fixed Mindset: Focus more on performance and outcomes and not looking bad. 

    * Growth Mindset:  On the process of getting better.

---
#### Key Ingredients to Growth:


1. Effort -

    * Fixed Mindset: Seen as not necessary or useful.

    * Growth Mindset:  Useful- leads to further growth.
 
4. Challenges -

    * Fixed Mindset: Back down & avoid- frame it as a threat.

    * Growth Mindset:  Embrace & persevere- frame it as an opportunity.
 

5. Mistakes - 

    * Fixed Mindset: Hate them and get discouraged, avoid them.

    * Growth Mindset:  Use them to learn.

6. Feedback -

    * Fixed Mindset: Not helpful, get defensive, take it personally.

    * Growth Mindset:  Appreciate it & use it.

The four key ingredients to growth can all be mapped out to the two beliefs and focus as our mindset fuels our drive to learn and grow.


#### Question 4
What are your key takeaways from the video to take action on?

You are not just one or the other. Mindset is a spectrum. At different times, on different days, you might slip into the other mindset. As we understand our mindset, we can identify the cause of it whether it is beliefs or focus and then take action to change it.


## 3. Understanding Internal Locus of Control

How to stay motivated Video - The Locus Rule - 5:47 minutes - [https://www.youtube.com/watch?v=8ZhoeSaPF-k](https://www.youtube.com/watch?v=8ZhoeSaPF-k)

#### Question 5
What is the Internal Locus of Control? What is the key point in the video?

Locus of Control is essentially the degree to which you believe you have control over your life.

Internal Locus of Control is the belief that factors one can control are leading to one's success in a field. Internal Locus of Control is the key to staying motivated over the long term as one believes the hard work they put in work leads to their success over time. Blaming factors outside one's control can make you less motivated to return to that task. 

## 4. How to build a Growth Mindset

How to build a Growth Mindset Video - 10:22 minutes - [https://www.youtube.com/watch?v=9DVdclX6NzY](https://www.youtube.com/watch?v=9DVdclX6NzY)

#### Question 6
Paraphrase (summarize) the video in a few lines in your own words.

Having belief in your abilities ultimately helps you achieve the goals you set out to follow. Having a negative or fixed mindset leads you to believe you're not good enough for something which stagnates your growth. You should always be keen on learning and improvement in every task. Do not feel uncomfortable being bad at something while you're new to it. Seeking to grow means you are open to struggles, failures and setbacks. Honour the struggle and challenges. Think of these hardships as opportunities you can learn from and develop tools in your brain to later tackle similar situations easily. 

#### Question 7
What are your key takeaways from the video to take action on?

See hardships as opportunities and honour the struggles. Believe in a long-term growth mindset and enjoy the tasks at hand without worrying about the result. You are much more likely to persevere when you face difficulty with an open mindset and seek lessons from it.

## 5. Mindset - A MountBlue Warrior Reference Manual

[Link to Google Docs - Mindset](https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk)

#### Question 8
What are one or more points that you want to take action on from the manual? (Maximum 3)



1. I will follow the steps required to solve problems:
Relax
Focus - What is the problem? What do I need to know to solve it?
Understand - Documentation, Google, Stack Overflow, GitHub Issues, Internet
Code
Repeat

2. I will not leave my code unfinished till I complete the following checklist:
Make it work.
Make it readable.
Make it modular.
Make it efficient.

3. I will be focused on mastery till I can do things half asleep. If someone wakes me up at 3 AM in the night, I will calmly solve the problem like James Bond and go to sleep. 